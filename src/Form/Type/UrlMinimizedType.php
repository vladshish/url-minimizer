<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Url as UrlValidator;
use App\Entity\DateTimeExt;

/**
 * Class UrlMinimizedType
 * @package App\Form\Type
 */
class UrlMinimizedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'url',
                TextType::class,
                ['constraints' => [new UrlValidator()]]
            )
            ->add(
                'date_expire',
                DateType::class,
                [
                    'data' => new DateTimeExt('now'),
                    'widget' => 'single_text',
                ]
            )
            ->add('submit', SubmitType::class)
        ;
    }
}
