<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\MinimizeUrl;

class MinimizeUrlRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MinimizeUrl::class);
    }

    public function isExists(MinimizeUrl $minimizeUrl) : bool
    {
        return (bool)$this->findOneBy(['hash' => $minimizeUrl->getHash()]);
    }

    public function findByUserId(int $userId): array
    {
        return $this->findBy(['userId' => $userId]);
    }

    public function save(MinimizeUrl $minimizeUrl)
    {
        $this->getEntityManager()
            ->persist($minimizeUrl);
        $this->getEntityManager()
            ->flush($minimizeUrl);
    }

    public function remove(MinimizeUrl $minimizeUrl)
    {
        $this->getEntityManager()
            ->remove($minimizeUrl);
        $this->getEntityManager()
            ->flush($minimizeUrl);
    }
}
