<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\MinimizeUrl;
use App\Form\Type\UrlMinimizedType;
use App\Repository\MinimizeUrlRepository;
use App\Service\Minimizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MinimizeUrlController extends AbstractController
{
    protected MinimizeUrlRepository $minimizeUrlRepo;

    public function __construct(MinimizeUrlRepository $minimizeUrlRepo)
    {
        $this->minimizeUrlRepo = $minimizeUrlRepo;
    }

    #[Route('/minimize-url/save', name: 'minimize_url_save')]
    public function index(Request $request)
    {
        $minimizeUrl = new MinimizeUrl();
        $form = $this->createForm(UrlMinimizedType::class, $minimizeUrl);
        $form->handleRequest($request);
        $userId = $request
            ->cookies
            ->get('user_id');

        if ($form->isSubmitted() && $form->isValid()) {
            $minimizeUrl->setUserId($userId);
            $minimizeUrl->setCount(0);
            $minimizeUrl->setHash(
                Minimizer::minimize($minimizeUrl)
            );

            if ($this->minimizeUrlRepo->isExists($minimizeUrl)) {
               return $this->redirectToRoute('home');
            }

            $this->minimizeUrlRepo->save($minimizeUrl);
        }

        $this->flashErrors($data['errors'] ?? []);
        return $this->redirectToRoute('home');
    }

    protected function flashErrors(array $errors)
    {
        foreach ($errors as $message) {
            $this->addFlash('error', $message);
        }
    }
}
