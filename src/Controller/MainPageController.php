<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\MinimizeUrlRepository;
use App\Form\Type\UrlMinimizedType;
use App\Service\UserIdGenerator;
use App\Entity\MinimizeUrl;

/**
 * Class MainPageController
 * @package App\Controller
 */
class MainPageController extends AbstractController
{
    protected MinimizeUrlRepository $minimizeUrlRepo;

    public function __construct(
        MinimizeUrlRepository $minimizeUrlRepo
    ) {
        $this->minimizeUrlRepo = $minimizeUrlRepo;
    }

    #[Route('/', name: 'home')]
    public function index(Request $request)
    {
        $minimizeUrl = new MinimizeUrl();
        $form = $this->createForm(UrlMinimizedType::class, $minimizeUrl);
        $form->handleRequest($request);
        $userId = (int) $request
            ->cookies
            ->get('user_id', -1);

        $data = $this->minimizeUrlRepo->findByUserId($userId);

        $this->flashErrors($data['errors'] ?? []);

        $response = $this->render('home-page/index.html.twig', [
            'form' => $form->createView(),
            'minimizesUrls' => $data['minimizesUrls'] ?? [],
        ]);

        $this->setUserId($request, $response);

        return $response;
    }

    protected function flashErrors(array $errors)
    {
        foreach ($errors as $message) {
            $this->addFlash('error', $message);
        }
    }

    protected function setUserId(Request $request, Response $response)
    {
        if ($request->cookies->get('user_id')) {
            return;
        }

        $response
            ->headers
            ->setCookie(
                new Cookie(
                    'user_id',
                    (string) UserIdGenerator::generate()
                )
            );
    }
}
