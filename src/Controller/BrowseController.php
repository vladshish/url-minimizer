<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\MinimizeUrlRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BrowseController extends AbstractController
{
    protected MinimizeUrlRepository $minimizeUrlRepo;

    public function __construct(MinimizeUrlRepository $minimizeUrlRepository)
    {
        $this->minimizeUrlRepo = $minimizeUrlRepository;
    }

    #[Route('/minimize-url/browse', name: 'minimize_url_browse')]
    public function index()
    {
        return $this->render('minimizes-urls/browse.html.twig', [
            'minimizesUrls' => $this->minimizeUrlRepo->findAll(),
        ]);
    }
}
