<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\MinimizeUrl;
use App\Repository\MinimizeUrlRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RedirectController
 * @package App\Controller
 */
class RedirectController extends AbstractController
{
    protected MinimizeUrlRepository $minimizeUrlRepo;

    public function __construct(
        MinimizeUrlRepository $minimizeUrlRepo
    ) {
        $this->minimizeUrlRepo = $minimizeUrlRepo;
    }

    #[Route('/{hash}', name: 'redirect_index')]
    public function index(string $hash)
    {
        $minimizeUrl = $this->minimizeUrlRepo
            ->findOneBy(['hash' => $hash]);

        $this->validateMinimizeUrlEntity($minimizeUrl);
        $this->setStatistic($minimizeUrl);

        return $this->redirect(
            $minimizeUrl->getUrl() ?? '/'
        );
    }

    protected function validateMinimizeUrlEntity(
        ?MinimizeUrl $minimizeUrl
    ): void {
        if (!$minimizeUrl) {
            throw $this->createNotFoundException();
        }

        if ($minimizeUrl->isExpired()) {
            $this->minimizeUrlRepo->remove($minimizeUrl);

            throw $this->createNotFoundException();
        }
    }

    protected function setStatistic(MinimizeUrl $minimizeUrl): void
    {
        $minimizeUrl->incrementPageView();
        $this->minimizeUrlRepo->save($minimizeUrl);
    }
}
